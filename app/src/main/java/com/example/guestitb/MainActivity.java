package com.example.guestitb;


import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // DETAIL
        /*Fragment missedFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (missedFragment == null) {
            missedFragment = new MissedAttendanceFragment();
            fragmentTransaction.add(R.id.fragment_container, missedFragment);
            fragmentTransaction.commit();
        }*/


        Fragment missedAttendanceList =  getSupportFragmentManager().findFragmentById(R.id.fragment_container);

        if (missedAttendanceList == null) {
            missedAttendanceList = new MissedAttendanceListFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, missedAttendanceList).commit();
        }

    }
}