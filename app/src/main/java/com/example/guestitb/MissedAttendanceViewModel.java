package com.example.guestitb;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MissedAttendanceViewModel extends ViewModel {
    List<MissedAttendanceModel> missedAttendances = new ArrayList<MissedAttendanceModel>();

    public MissedAttendanceViewModel() {
        MissedAttendanceModel missedAttendance = new MissedAttendanceModel();

        for (int i = 0; i < 100; i++) {
            String name = "Name "+ i;
            String nameModule = "Module " + i;
            missedAttendance.setNameStudent(name);
            missedAttendance.setDate(new Date());
            missedAttendance.setJustified(true);

            missedAttendance.setModuleName(nameModule);

            missedAttendances.add(missedAttendance);
        }
    }
}
