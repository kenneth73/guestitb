package com.example.guestitb;

import java.util.Date;

public class MissedAttendanceModel {
    String nameStudent;
    String moduleName;
    Date date;
    boolean isJustified;

    /*public MissedAttendanceModel(String nameStudent, String moduleName, boolean isJustified) {
        this.nameStudent = nameStudent;
        this.moduleName = moduleName;
        this.isJustified = isJustified;
        this.date = new Date();
    }*/

    public MissedAttendanceModel() {
        this.nameStudent = "";
        this.moduleName = "";
        this.date = new Date();
        this.isJustified = false;
    }

    public String getNameStudent() {
        return nameStudent;
    }

    public void setNameStudent(String nameStudent) {
        this.nameStudent = nameStudent;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isJustified() {
        return isJustified;
    }

    public void setJustified(boolean justified) {
        isJustified = justified;
    }

}