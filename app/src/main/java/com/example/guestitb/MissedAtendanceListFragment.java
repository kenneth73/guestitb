package com.example.guestitb;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MissedAtendanceListFragment extends Fragment {
    RecyclerView missedAttendanceList;

    MissedAttendanceViewModel missedAttendanceViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        missedAttendanceViewModel = new MissedAttendanceViewModel();

        View v = inflater.inflate(R.layout.missed_atendance_list_fragment, container, false);

        missedAttendanceList= v.findViewById(R.id.missed_atendance_recicler);

        missedAttendanceList.setLayoutManager(new LinearLayoutManager(getContext()));

        MissedAttendanceAdapter adapter = new MissedAttendanceAdapter(missedAttendanceViewModel.missedAttendances);

        missedAttendanceList.setAdapter(adapter);

        return v;
    }
}