package com.example.guestitb;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.util.Date;


public class MissedAttendanceFragment extends Fragment{

    TextView nameText ;
    EditText inputName ;
    TextView detailTitle;
    Spinner modulesSpinner;
    Button btnDate;
    Button btnAdd;
    CheckBox isJustify;

    String[] modules;
    String module;
    String missedDate;
    boolean justify;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.missed_attendance_fragment, container, false);
        nameText = v.findViewById(R.id.text_name);
        inputName = v.findViewById(R.id.input_name);
        detailTitle = v.findViewById(R.id.title_detail);
        modulesSpinner = v.findViewById(R.id.modules_spinner);
        btnDate = v.findViewById(R.id.btn_date);
        btnAdd = v.findViewById(R.id.btn_add);
        isJustify = v.findViewById(R.id.checkbox_justify);

        //set text btnDate
        Date dateNow = new Date();
        String dateStr = dateNow.toString();
        btnDate.setText(dateStr);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.modules_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        modulesSpinner.setAdapter(adapter);

        modules = getResources().getStringArray(R.array.modules_array);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = inputName.getText().toString();
                if (!name.equals("")){
                    justify = isJustify.isChecked();
                    missedDate = btnDate.getText().toString();
                    AlertDialog dialog = createSimpleDialog();
                    dialog.show();
                } else {
                    Toast.makeText(getContext(), "Name is required!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        modulesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View vies, int position, long id) {
                module = modules[position];
                Toast.makeText(getContext(), module, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

        return v;

    }

    /**
     * Create a simple alert dialog
     * @return New dialogue
     */
    public AlertDialog createSimpleDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

        String message = "The student "+inputName.getText().toString() + " has missed " + module.toString() + " on " + missedDate;

        if (justify) {
            message += " whith justification";
        } else {
            message+= " whithout justification";
        }

        alert.setTitle("Missed Attendance Created")
                .setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Toast.makeText(MainActivity.this, "CurrentIndex= "+ quizViewModel.currentIndex, Toast.LENGTH_LONG);
                            }
                        })
                .setNegativeButton("",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
        return alert.create();
    }



}

